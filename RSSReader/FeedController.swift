//
//  FeedController.swift
//  RSSReader
//
//  Created by Tianyong Tang on 2019/1/2.
//  Copyright © 2019 Tianyong Tang. All rights reserved.
//

import Foundation
import CoreData
import FeedKit

class FeedController {

    private init() {
        /* nop */
    }

    weak var delegate: FeedControllerDelegate?

    static let shared = FeedController()

    private let container: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "RSSReader")

        container.loadPersistentStores { (persistentStoreDescription, error) in
            if let error = error as NSError? {
                fatalError(error.localizedDescription)
            }
        }

        return container
    }()

    private var viewContext: NSManagedObjectContext {
        return container.viewContext
    }

    func fetchFeeds() -> [Feed] {
        let context = viewContext
        let request: NSFetchRequest<Feed> = Feed.fetchRequest()

        do {
            return try context.fetch(request)
        } catch {
            // For simplicity, ignore exception.
            return []
        }
    }

    // MARK: Add Feed

    func addFeed(_ atomFeed: AtomFeed) {
        let context = viewContext
        let feed = Feed(context: context)

        feed.title = atomFeed.title

        if let entries = atomFeed.entries {
            entries.forEach { entry in
                guard
                    let title = entry.title,
                    let summary = entry.summary?.value,
                    let href = entry.links?.first?.attributes?.href,
                    let url = URL(string: href)
                else {
                    return
                }

                let feedItem = FeedItem(context: context)

                feedItem.url = url
                feedItem.title = title
                feedItem.summary = summary

                feedItem.feed = feed
            }
        }

        save()
    }

    func addFeed(_ jsonFeed: JSONFeed) {
        let context = viewContext
        let feed = Feed(context: context)

        feed.title = jsonFeed.title

        if let items = jsonFeed.items {
            items.forEach { entry in
                guard
                    let title = entry.title,
                    let summary = entry.summary,
                    let urlString = entry.url,
                    let url = URL(string: urlString)
                else {
                    return
                }

                let feedItem = FeedItem(context: context)

                feedItem.url = url
                feedItem.title = title
                feedItem.summary = summary

                feedItem.feed = feed
            }
        }

        save()
    }

    func addFeed(_ rssFeed: RSSFeed) {
        let context = viewContext
        let feed = Feed(context: context)

        feed.title = rssFeed.title

        if let items = rssFeed.items {
            items.forEach { entry in
                guard
                    let title = entry.title,
                    let summary = entry.description,
                    let urlString = entry.link,
                    let url = URL(string: urlString)
                else {
                    return
                }

                let feedItem = FeedItem(context: context)

                feedItem.url = url
                feedItem.title = title
                feedItem.summary = summary

                feedItem.feed = feed
            }
        }

        save()
    }

    private func save() {
        do {
            try viewContext.save()

            if let delegate = delegate {
                delegate.feedDidChange(self)
            }
        } catch _ {
            // For simplicity, ignore the exception.
        }
    }

}

protocol FeedControllerDelegate: NSObjectProtocol {

    func feedDidChange(_ feedController: FeedController)

}
