//
//  FeedsTableViewController.swift
//  RSSReader
//
//  Created by Tianyong Tang on 2018/12/31.
//  Copyright © 2018 Tianyong Tang. All rights reserved.
//

import UIKit

/**
 View controller for managing and showing feeds.
 */
class FeedsTableViewController: UITableViewController {

    private var feeds: [Feed] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        FeedController.shared.delegate = self

        reloadData()
    }

    private func reloadData() {
        feeds = FeedController.shared.fetchFeeds()
        tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feeds.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let feed = feeds[indexPath.row]

        let cell = (
            tableView.dequeueReusableCell(withIdentifier: "Feed") ??
            tableView.dequeueReusableCell(withIdentifier: "Feed", for: indexPath)
        )

        cell.textLabel?.text = feed.title

        return cell
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "Detail":
            guard
                let cell = sender as? UITableViewCell,
                let indexPath = tableView.indexPath(for: cell),
                let viewController = segue.destination as? FeedItemsTableViewController
            else {
                return
            }

            let feed = feeds[indexPath.row]

            viewController.feed = feed
        default:
            break
        }
    }

}

extension FeedsTableViewController: FeedControllerDelegate {

    func feedDidChange(_ feedController: FeedController) {
        reloadData()
    }

}
