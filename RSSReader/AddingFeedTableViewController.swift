//
//  AddingFeedTableViewController.swift
//  RSSReader
//
//  Created by Tianyong Tang on 2019/1/1.
//  Copyright © 2019 Tianyong Tang. All rights reserved.
//

import UIKit
import FeedKit

/**
 View controller for adding feed.
 */
class AddingFeedTableViewController: UITableViewController {

    @IBOutlet weak var urlTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        urlTextField.becomeFirstResponder()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        view.endEditing(true)
    }

    private func handleParsingResult(_ result: FeedKit.Result) {
        switch result {
        case .atom(let feed):
            FeedController.shared.addFeed(feed)
            dismiss(animated: true, completion: nil)
        case .json(let feed):
            FeedController.shared.addFeed(feed)
            dismiss(animated: true, completion: nil)
        case .rss(let feed):
            FeedController.shared.addFeed(feed)
            dismiss(animated: true, completion: nil)
        case .failure:
            // For simplicity, break silently.
            break
        }
    }

    /**
     Validate and add feed.
     */
    @IBAction func addFeed(_ sender: UIBarButtonItem) {
        guard
            let urlString = urlTextField.text,
            let url = URL(string: urlString)
        else {
            // For simplicity, return silently.
            return
        }

        let feedParser = FeedParser(URL: url)

        feedParser.parseAsync(queue: .global(qos: .userInitiated)) { [weak self] result in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.handleParsingResult(result)
            }
        }
    }

    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

}
