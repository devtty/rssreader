//
//  FeedItemsTableViewController.swift
//  RSSReader
//
//  Created by Tianyong Tang on 2019/1/3.
//  Copyright © 2019 Tianyong Tang. All rights reserved.
//

import UIKit
import SafariServices

class FeedItemsTableViewController: UITableViewController {

    var feed: Feed?

    var feedItems: [FeedItem] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        reloadData()
    }

    func reloadData() {
        if
            let feed = feed,
            let feedItems = feed.items?.array as? [FeedItem]
        {
            self.feedItems = feedItems
        } else {
            self.feedItems = []
        }

        tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedItems.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let feedItem = feedItems[indexPath.row]

        let cell = (
            tableView.dequeueReusableCell(withIdentifier: "FeedItem") ??
            tableView.dequeueReusableCell(withIdentifier: "FeedItem", for: indexPath)
        )

        cell.textLabel?.text = feedItem.title

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let feedItem = feedItems[indexPath.row]

        guard let feedItemURL = feedItem.url else {
            return
        }

        let configuration = SFSafariViewController.Configuration()
        let viewController = SFSafariViewController(url: feedItemURL, configuration: configuration)

        present(viewController, animated: true)
    }

}
